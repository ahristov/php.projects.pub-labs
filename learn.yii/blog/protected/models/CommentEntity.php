<?php

/**
 * This is the model class for table "{{comment}}".
 */
class CommentEntity extends Comment
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	const STATUS_PENDING=1;
	const STATUS_APPROVED=2;
	
	

}

