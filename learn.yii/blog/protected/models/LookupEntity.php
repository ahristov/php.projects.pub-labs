<?php

/**
 * This is the model class for table "{{lookup}}".
 * We can call:
 * LookupEntity::items(LookupEntity::TYPE_POST_STATUS)
 * -or-
 * LookupEntity::item(LookupEntity::TYPE_POST_STATUS, PostEntity::STATUS_PUBLISHED)
 */
class LookupEntity extends Lookup
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Lookup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	const TYPE_POST_STATUS='PostStatus';
	const TYPE_COMMENT_STATUS='CommentStatus';


	private static $_items=array();
	
	public static function items($type) 
	{
		if (!isset(self::$_items[$type]))
			self::loadItems($type);
			
		return self::$_items[$type];
	}
	
	public static function item($type,$code)
	{
		if (!isset(self::$_items[$type]))
			self::loadItems($type);
		
		return isset(self::$_items[$type][$code]) 
			? self::$_items[$type][$code] 
			: false;
	}

	public static function loadItems($type)
	{
		self::$_items[$type]=array();
		
		$models=self::model()->findAll(array(
			'condition'=>'type=:type',
			'params'=>array(':type'=>$type),
			'order'=>'position',
		));
		
		foreach($models as $model)
			self::$_items[$type][$model->code]=$model->name;
			
	}

}

