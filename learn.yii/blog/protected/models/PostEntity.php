<?php

/**
 * This is the model class for table "{{post}}".
 */
class PostEntity extends Post
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, status', 'required'),
			array('title', 'length', 'max'=>128),
			array('status', 'in', 'range'=>array(1,2,3)), /* draft, published, archived */
			array('tags', 'match', 'pattern'=>'/^[\w\s,]+$/',
				'message'=>'Tags can only contain word characters.'),
			array('tags', 'normalizeTags'),
			array('title, status', 'safe', 'on'=>'search'),
		);
	}

	public function normalizeTags($attribute,$params)
	{
		$this->tags=TagEntity::array2string(array_unique(TagEntity::string2array($this->tags)));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'UserEntity', 'author_id'),
			'comments' => array(self::HAS_MANY, 'CommentEntity', 'post_id',
				'condition'=>'comments.status='.CommentEntity::STATUS_APPROVED,
				'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'CommentEntity', 'post_id',
				'condition'=>'status='.CommentEntity::STATUS_APPROVED),
		);
	}
	
	public function getUrl()
	{
		return Yii::app()->createUrl('post/view', atrray(
			'id'=>$this->id,
			'title'=$this->title,
		));
	}
	
}

