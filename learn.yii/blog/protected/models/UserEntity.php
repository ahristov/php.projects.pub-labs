<?php

/**
 * This is the model class for table "{{user}}".
 */
class UserEntity extends User
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function hashPassword($password, $salt)
	{
		return md5($salt.$password);
	}	
	
	public function validatePassword($password)
	{
		return $this->hashPassword($password, $this->salt) === $this->password;
	}
	

}

