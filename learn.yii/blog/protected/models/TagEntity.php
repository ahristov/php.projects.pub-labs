<?php

/**
 * This is the model class for table "{{tag}}".
 */
class TagEntity extends Tag
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
